package com.sychev.logParser;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


/**
 * @author Maksim Sychev
 * @task a(3), b(3), c(3), d(3)
 *
 */
public class LogMapClass extends Mapper<LongWritable, Text, Text, IntWritable>{
    private final static IntWritable one = new IntWritable(1);

    /**
     * enum for Counters
     */
    enum Counters {
        MALFORMEDROW,
        TOTALROWS
    }

    protected Text browser = new Text();

    /**
     *
     * @param key
     * @param value
     * @param context
     * @throws IOException
     * @throws InterruptedException
     *
     * Find browser in logs
     */
    @Override
    public void map(LongWritable key, Text value,
                       Mapper.Context context)
            throws IOException, InterruptedException {
        String line = value.toString();


        context.getCounter(Counters.TOTALROWS).increment(1L);


        if (!isAsciiString(line)) {
            context.getCounter(Counters.MALFORMEDROW).increment(1L);
        }

        int browserStart = line.indexOf("\" \"");
        if (browserStart!=-1){
            int browserEnd = line.indexOf("/", browserStart+3);
            browser.set(line.substring(browserStart+3, browserEnd));
            context.write(browser,one);
        }

    }

    /**
     *
     * @param line
     * @return
     * Check if every char in string is ASCII (Malformed rows)
     *
     */

    private boolean isAsciiString(String line) {
        return line.matches("\\A\\p{ASCII}*\\z");
    }
}

package com.sychev.logParser;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class LogReduceClass extends Reducer<Text, IntWritable, Text, IntWritable>{
    /**
     *
     * @param key
     * @param values
     * @param context
     * @throws IOException
     * @throws InterruptedException
     *
     * Count sum of browsers
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values,
                          Context context)
            throws IOException, InterruptedException {

        int sum = 0;

        for (IntWritable val : values
        ) {
            sum += val.get();
        }

        context.write(key, new IntWritable(sum));
    }
}
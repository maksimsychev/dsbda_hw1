// скормить методу isAscii текст с символом 0x00FF
// разобраться с кодом ниже

package com.sychev.logParser;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class logParserTest {
    private LogMapClass mapper;
    private Context context;
    private Counter counterTotalRows;
    private Counter counterMalformedRows;
    private IntWritable one;
    private String malformedRow;
    private String rowWithoutBrowser;
    private String row;
    private String testBrowser;
    private char notAsciiChar;

    @Before
    public void init() throws IOException, InterruptedException {
        mapper = new LogMapClass();
        context = mock(Context.class);
        mapper.browser = mock(Text.class);
        one = new IntWritable(1);
        notAsciiChar = 0x00FF;
        testBrowser = "Mozilla";
        malformedRow = "\"http://www.example.com/\" \"Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_1_3 like Mac OS X; en-us) AppleWebKit/528.18"+notAsciiChar;
        row = "\"http://www.example.com/\" \"Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_1_3 like Mac OS X; en-us) AppleWebKit/528.18";
        rowWithoutBrowser = "99.168.127.53 - - [20/May/2010:07:34:13 +0100] \";GET /media/img/m-inact.gif HTTP/1.1\" 200 2571\n";

        counterMalformedRows = mock(Counter.class);
        counterTotalRows = mock(Counter.class);

        when(context.getCounter(LogMapClass.Counters.MALFORMEDROW)).thenReturn(counterMalformedRows);
        when(context.getCounter(LogMapClass.Counters.TOTALROWS)).thenReturn(counterTotalRows);
    }

    /**
     *
     *
     * @throws IOException
     * @throws InterruptedException
     *
     * Test if counter of total rows incremented, output Text var set to Mazilla
     * and output key/value set Mazilla/one.
     */
    @Test
    public void testWithBrowser() throws IOException, InterruptedException {
        mapper.map(new LongWritable(1L), new Text(row), context);

        InOrder inOrder = inOrder(mapper.browser, context, counterTotalRows);
        inOrder.verify(counterTotalRows).increment(1);
        inOrder.verify(mapper.browser).set(eq(testBrowser));
        inOrder.verify(context).write(eq(mapper.browser), eq(one));
    }

    /**
     *
     * @throws IOException
     * @throws InterruptedException
     *
     * Use line of logs without browser information
     * Test if browser wasn't set to Mazilla and output key/value wasn't written
     */
    @Test
    public void testWithoutBrowser() throws IOException, InterruptedException {
        mapper.map(new LongWritable(1L), new Text(rowWithoutBrowser), context);

        InOrder inOrder = inOrder(mapper.browser, context, counterTotalRows);
        inOrder.verify(mapper.browser, never()).set(eq(testBrowser));
        inOrder.verify(context, never()).write(eq(mapper.browser), eq(one));
    }

    /**
     *
     * @throws IOException
     * @throws InterruptedException
     *
     * Use line with not ASCII char and with browser info
     * Test if counterTotalRows and counterMalformedRows was incremented,
     * browser was set to Mazilla and output key/value was written
     */
    @Test
    public void testMalformedRow() throws IOException, InterruptedException {
        mapper.map(new LongWritable(1L), new Text(malformedRow), context);

        InOrder inOrder = inOrder(mapper.browser, context,counterTotalRows ,counterMalformedRows);
        inOrder.verify(counterTotalRows).increment(1);
        inOrder.verify(counterMalformedRows).increment(1);
        inOrder.verify(mapper.browser).set(eq(testBrowser));
        inOrder.verify(context).write(eq(mapper.browser), eq(one));
    }
}
